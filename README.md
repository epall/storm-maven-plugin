# Storm Maven Plugin

Storm utilities integrated into your Maven workflow.

## Credits

Developed by Eric Allen <eric.allen@adroll.com> while working at AdRoll, Inc.
