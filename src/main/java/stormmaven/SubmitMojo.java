package stormmaven;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.classworlds.realm.ClassRealm;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by ericallen on 5/28/14.
 */

@Mojo(name = "submit", defaultPhase = LifecyclePhase.DEPLOY)
public class SubmitMojo extends AbstractMojo {
  @Parameter(property = "topology", required = true)
  private String topology;

  @Parameter(property = "nimbus-host", defaultValue = "localhost")
  private String nimbusHost;

  @Parameter(property = "topology-class", required = true)
  private String topologyClass;

  @Parameter(property = "main-arguments")
  private String mainArguments;

  @Parameter(property = "wait", defaultValue = "30")
  private int wait;

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    getLog().info("Submitting topology " + topology);
    System.setProperty("storm.options", "nimbus.host=" + nimbusHost);
    System.setProperty("topology.name", topology);

    setStormJar();

    loadClasspath();

    invokeMain();
  }

  private void setStormJar() {
    final MavenProject project = (MavenProject) getPluginContext().get("project");

    File targetDir = new File(project.getBuild().getDirectory());
    File targetJar = new File(targetDir, project.getArtifactId() + ".jar");

    System.setProperty("storm.jar", targetJar.getAbsolutePath());
  }

  private void invokeMain() throws MojoExecutionException, MojoFailureException {
    long start = System.currentTimeMillis();

    while (System.currentTimeMillis() < start + (wait * 1000)) {
      try {
        Class topology = Class.forName(topologyClass);

        String[] args = null;

        if (mainArguments != null) {
          args = mainArguments.split(" ");
        }

        Method main = topology.getMethod("main", String[].class);
        main.invoke(null, new Object[]{args});
        return;
      } catch (ClassNotFoundException e) {
        throw new MojoExecutionException("topologyClass not found", e);
      } catch (NoSuchMethodException e) {
        throw new MojoExecutionException("topologyClass has no main method", e);
      } catch (InvocationTargetException e) {
        Throwable cause = e.getCause();
        if (cause.getMessage().contains("already exists on cluster")) {
          getLog().info("Topology already running on cluster. Waiting for it to die...");
          try { Thread.sleep(5000); } catch (InterruptedException e1) {}
        } else {
          throw new MojoExecutionException("Error invoking main on topologyClass", e.getCause());
        }
      } catch (IllegalAccessException e) {
        throw new MojoExecutionException("Error invoking main on topologyClass", e);
      }
    }
    // timed out and never ran
    throw new MojoFailureException("Topology still running after " + wait + " seconds");
  }

  private void loadClasspath() throws MojoExecutionException {
    final PluginDescriptor pluginDescriptor = (PluginDescriptor) getPluginContext().get("pluginDescriptor");
    final ClassRealm classRealm = pluginDescriptor.getClassRealm();
    final MavenProject project = (MavenProject) getPluginContext().get("project");
    final File targetDir = new File(project.getBuild().getDirectory());
    final File targetJar = new File(targetDir, project.getArtifactId() + ".jar");

    try {
      classRealm.addURL(targetJar.toURI().toURL());
    }
    catch (MalformedURLException e) {
      throw new MojoExecutionException("Error adding to classpath", e);
    }
  }
}
