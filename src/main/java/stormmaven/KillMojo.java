package stormmaven;

import backtype.storm.generated.NotAliveException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name="kill")
public class KillMojo extends AbstractMojo {
  @Parameter(property = "topology", required = true)
  private String topology;

  @Parameter(property = "nimbus-host", defaultValue = "localhost")
  private String nimbusHost;

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    getLog().info("Killing topology " + topology);
    System.setProperty("storm.options", "nimbus.host="+nimbusHost);
    try {
      backtype.storm.command.kill_topology.main(new String[]{topology});
    } catch (Exception e) {
      // LOL Java is angry because Clojure can throw Exceptions as unchecked exceptions
      if (e instanceof NotAliveException) {
        //throw new MojoFailureException(e.getMessage());
        throw new MojoFailureException(((NotAliveException) e).get_msg());
      } else {
        throw new MojoExecutionException("unexpected error trying to kill topology", e);
      }
    }
  }
}